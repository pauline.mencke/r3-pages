---
layout: default
date: 2020-10-15
order: -1
title: "Agent-based SEIR-ICU model for Luxembourg"
permalink: /frozen/q3g1-7a85
published: true
doi: 10.17881/q3g1-7a85
type: other
---

{% rtitle {{ page.title }} %}
Atte Aalto, Laurent Mombaerts, Laurent Heirendt, Christophe Trefois, Daniele Proverbio, Françoise Kemp, Jorge Gonçalves, Alexander Skupin
{% endrtitle %}


{% rgridblock a-unique-id %}

{% rblock Source code %}
Source code is hosted on Gitlab [Gitlab](https://gitlab.lcsb.uni.lu/SCG/seir-icu).
{% endrblock %}

{% rblock Artificial data %}
The used real data cannot be shared due to data protection reasons. An artificial, generated social network data can be found here (households and regional information is not included).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Other
{% endrfooter %}
