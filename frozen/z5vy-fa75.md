---
layout: default
date: 2020-04-08
order: -1
title: "Clustering and visualizing huge-scale cytometry datasets with GigaSOM.jl"
permalink: /frozen/z5vy-fa75
published: true
doi: 10.17881/z5vy-fa75
type: manuscript
manuscript: 10.1093/gigascience/giaa127
redirect_from:
  - /frozen/gigasom
  - /frozen/gigasom/
---

{% rtitle {{ page.title }} %}

Miroslav Kratochvíl, Oliver Hunewald, Laurent Heirendt, Vasco Verissimo, Jiří Vondrášek, Venkata P. Satagopam, Reinhard Schneider, Christophe Trefois, Markus Ollert
<center>
<img src="{{ "assets/z5vy-fa75/logo-GigaSOM.jl.png" | relative_url }}" width="90%" />
</center>
{% endrtitle %}

{% rgridblock a-unique-id %}
{% rblock  Raw data | fas fa-video %}
The used datasets are available on FlowRepository, under accession IDs [FR-FCM-ZZPH](http://flowrepository.org/id/FR-FCM-ZZPH)
and [FR-FCM-ZYX9](http://flowrepository.org/id/FR-FCM-ZYX9).
{% endrblock %}

{% rblock source code %}
**GigaSOM.jl** source code is hosted on [Github](https://github.com/LCSB-BioCore/GigaSOM.jl).
**GigaScatter.jl**, used for dataset rasterization, is also hosted on [Github](https://github.com/LCSB-BioCore/GigaScatter.jl).

{% endrblock %}

{% rblock documentation | fas fa-book %}
The documentation of **GigaSOM.jl** is available [here](https://git.io/GigaSOM.jl).

{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
