---
layout: default
date: 2016-10-18
order: -1
title: "IMP: a pipeline for reproducible reference-independent integrated metagenomic and metatranscriptomic analyses"
permalink: /frozen/21vm-kj21
published: true
doi: 10.17881/21vm-kj21
type: manuscript
manuscript: 10.1186/s13059-016-1116-8
redirect_from:
 - /frozen/imp
---

{% rtitle {{ page.title }} %}

Please cite the article on [Genome Biology](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-1116-8).

Shaman Narayanasamy, Yohan Jarosz, Emilie E. L. Muller, Anna Heintz-Buschart, Malte Herold, Anne Kaysen, Cédric C. Laczny, Nicolás Pinel, Patrick May and Paul Wilmes*
{% endrtitle %}

{% rgridblock a-unique-id %}
{%  rblock Website | fas fa-globe %}
The **IMP** website hosted within *R3lab* frame is available [here](https://r3lab.uni.lu/web/imp).
{% endrblock %}

{%  rblock Source code | fas fa-code %}
The source code of IMP is available on the [LCSB Gitlab](https://gitlab.lcsb.uni.lu/IMP/IMP) where you can traceback what have been done by the authors.
{% endrblock %}

{%  rblock Additionnal source code | fas fa-code %}
Some additionnal analysis made on the paper as also available on the [LCSB Gitlab](https://gitlab.lcsb.uni.lu/IMP/IMP_manuscript_analysis).
{% endrblock %}

{%  rblock  Workflow | fas fa-cogs %}
Snakemake, a reproducible workflow engine that allow us IMP users execute the same workflow for each analysis. From [Köster, Johannes and Rahmann, Sven. “Snakemake - A scalable bioinformatics workflow engine”. Bioinformatics 2012.](https://academic.oup.com/bioinformatics/article/28/19/2520/290322)
{% endrblock %}


{%  rblock  Tool repository | fas fa-dolly-flatbed %}
The **IMP** tool repository is hosted on the [LCSB webdav storage](https://webdav-r3lab.uni.lu/public/R3lab/IMP/). You will find some of the tools installed within IMP. IMP versions are stored under the dist folder.
{% endrblock %}


{%  rblock  Environment | fab fa-docker %}
All tools dependencies of IMP are frozen inside a docker container. The files used to build the container can be found on Gitlab and versioned tarballs of the containers are found under the [webdav](https://webdav-r3lab.uni.lu/public/R3lab/IMP/dist/).
{% endrblock %}


{%  rblock  R toolkit | fas fa-wrench %}
We use the [checkpoint](https://cran.r-project.org/web/packages/checkpoint/vignettes/checkpoint.html) library to ensure that all of the necessary R packages are installed with the correct versions.
{% endrblock %}


{%  rblock  Report | fas fa-book %}
 **IMP** report contains all information needed to reproduce the same workflow, from the version of **IMP** used to make the analysis to the configuration file used to start the workflow. Everything is condensed and organized within an HTML report. Some reports can be found [here](https://webdav-r3lab.uni.lu/public/R3lab/IMP/reports/).
 {% endrblock %}
{% endrgridblock %}



{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
