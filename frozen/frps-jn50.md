---
layout: default
date: 2022-07-24
order: -1
title: "ENA Dataset for Systematic characterization of human gut microbiome-secreted molecules by integrated multi-omics study"
permalink: /frozen/frps-jn50
published: true
doi: 10.17881/frps-jn50
type: data
---


{% rtitle {{ page.title }} %}

The human gut microbiome produces a complex mixture of biomolecules which interact with human physiology and play essential roles in health and disease. The crosstalk between micro-organisms and host cells is favored by different direct contacts, but also by the export of molecules through secretion systems and extracellular vesicles. The resulting molecular network, comprised of nucleic acids, (poly)peptide and small molecule moieties, has so far eluded systematic study. Here we present a methodological framework, optimized for the extraction of the microbiome-secreted extracellular complement, including nucleic acids, (poly)peptides and metabolites from flash-frozen human stool samples. Our method allows simultaneous isolation of individual biomolecular fractions from the same original stool sample, followed by specialized omic analyses. The resulting multi-omics data enables coherent data integration for the systematic characterization of this molecular complex. Our results demonstrate the distinctiveness of the different intracellular and extracellular biomolecular fractions, both in terms of their taxonomic and functional composition. This highlights the challenge of inferring the extracellular biomolecular complement of the gut microbiome based on single omics data. The developed methodological framework provides the foundation for systematically investigating mechanistic links between microbiome-secreted molecules, including those typically vesicle-associated, and their impact on host physiology in health and disease.



permalink: [doi:10.17881/frps-jn50](https://doi.org/10.17881/frps-jn50)

{% endrtitle %}


{% rblock Data access %}
The data for this study have been deposited in the European Nucleotide Archive (ENA) at EMBL-EBI under accession number [PRJEB44766](https://www.ebi.ac.uk/ena/browser/view/PRJEB44766).

{% endrblock %}



{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Dataset
{% endrfooter %}
