---
layout: default
date: 2022-09-01
order: -1
title: "Ice Recrystallization Inhibitors enable efficient cryopreservation of induced pluripotent stem cells: a functional and transcriptomic analysis"
permalink: /frozen/fpn5-th91
published: true
doi: 10.17881/fpn5-th91
type: manuscript
---

{% rtitle {{ page.title }} %}

Kathleen Mommaerts, Satoshi Okawa, Margaux Schmitt, Olga Kofanova, Tracey Turner, Robert Ben, Antonio Del Sol, Jens C. Schwamborn, Jason P. Acker, Fay Betsou
{% endrtitle %}

{% rblock Abstract %}

The successful use of human induced pluripotent stem
cells (iPSCs) for research or clinical applications requires the development of
robust, efficient and reproducible cryopreservation protocols. After
cryopreservation, the survival rate of iPSCs is poor and cell line dependent.
The aim of this study was to assess the use of Ice Recrystallization Inhibitors
(IRIs) for cryopreservation of human iPSCs. A toxicity screening study was
first performed to select the specific small-molecule IRI and concentration for
further evaluation. Then, a cryopreservation study compared the cryoprotective
efficiency of 15 mM IRIs in 5% or 10% DMSO-containing solution with CryoStor®
CS10. Three iPSC lines were cryopreserved as single-cell suspension in
cryopreservation solutions and post-thaw characteristics including pluripotency
and differential gene expression were assessed. We demonstrate the
fitness-for-purpose of 15 mM IRI in 5% DMSO as an efficient cryoprotective
solution for iPSCs in terms of their post-thaw recovery, viability,
pluripotency and transcriptomic changes. Given that this dataset is the first
one where mRNA sequencing has been used to identify expression changes
resulting from iPSCs cryopreservation, it has the potential to be used for
molecular mechanism analysis relating to cryopreservation. IRIs can reduce DMSO
concentrations, thereby improving the utility, effectiveness and efficiency of
cryopreservation.
{% endrblock %}

{% rblock Data | fas fa-database %}

The data is available <a href="https://webdav-r3lab.uni.lu/public/data/fpn5-th91/">here</a>.

{% endrblock %}

{% rblock  Scripts | fas fa-database %}
The scripts used to analyse the data are available [here](https://gitlab.lcsb.uni.lu/dvb/mommaerts_2022).
{% endrblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | not yet published
{% endrfooter %}
