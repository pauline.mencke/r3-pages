---
layout: default
date: 2021-02-01
order: -1
title: "Retrograde procedural memory in Parkinson's disease: a cross-sectional, case-control study"
permalink: /frozen/7bwb-aj16
published: true
doi: 10.17881/7bwb-aj16
type: manuscript
manuscript: 10.3233/JPD-213081
---

{% rtitle {{ page.title }} %}

Laure PAULY, Claire PAULY, Maxime HANSEN, Valerie E. SCHRÖDER, Armin RAUSCHENBERGER, Anja K. LEIST & Rejko KRÜGER on behalf of the NCER-PD Consortium

**Background:** The analysis of the procedural memory is particularly relevant in neurodegenerative disorders like Parkinson’s disease, due to the central role of the basal ganglia in procedural memory. It has been shown that anterograde procedural memory, the ability to learn a new skill, is impaired in Parkinson’s disease. However, retrograde procedural memory, the long-term retention and execution of skills learned in earlier life stages, has not yet been systematically investigated in Parkinson’s disease.

**Objective:** This study aims to investigate the retrograde procedural memory in people with Parkinson’s disease. We hypothesized that the retrograde procedural memory is impaired in people with Parkinson’s disease compared to an age- and gender-matched control group.

**Methods:** First, we developed an extended evaluation system based on the Cube Copying Test to distinguish the cube copying procedure, representing functioning of the retrograde procedural memory, and the final result, representing the visuo-constructive abilities. Development of the evaluation system included tests of discriminant validity.

**Results:** Comparing people with typical Parkinson’s disease (n=201) with age- and gender-matched control subjects (n=201), we identified cube copying performance to be significantly impaired in people with Parkinson’s disease (p=0.008). No significant correlation was observed between retrograde procedural memory and disease duration.
Conclusions: We demonstrated lower cube copying performance in people with Parkinson’s disease compared to control subjects, which suggests an impaired functioning of the retrograde procedural memory in Parkinson’s disease.
{% endrtitle %}

{% rgridblock %}

{% rblock Data | fas fa-database %}

The datasets for this manuscript are not publicly available as they are linked to the Luxembourg Parkinson’s Study and its internal regulations. Requests to access the datasets should be directed to the Executive Committee of NCER-PD, mean of contact via email: <a href="mailto:request.ncer-pd@uni.lu">request.ncer-pd@uni.lu</a>.

{% endrblock %}

{% rblock source code %}

The source code is available <a href="https://gitlab.lcsb.uni.lu/TNG/papers/RPMemory">here</a>.

{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
