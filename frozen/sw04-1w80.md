---
layout: default
date: 2022-07-15
order: -1
title: "Body-first subtype of Parkinson's disease with probable REM-sleep behaviour disorder is associated with non-motor dominant phenotype"
permalink: /frozen/sw04-1w80
published: true
doi: 10.17881/sw04-1w80
type: manuscript
---

{% rtitle {{ page.title }} %}

Pavelka L, Rauschenberger A, Landoulsi Z, Pachchek S, Marques T, Gomes CPC, Glaab E, May P, Krüger R on behalf of the NCER-PD Consortium

**Background**: The hypothesis of body-first vs brain-first subtype of PD has
been proposed with REM-Sleep behaviour disorder (RBD) defining the former. The
body-first PD presumes an involvement of the brainstem in the pathogenic
process with higher burden of autonomic dysfunction.

**Objective**: To identify distinctive clinical subtypes of idiopathic
Parkinson’s disease (iPD) in line with the formerly proposed concept of
body-first vs brain-first subtypes in PD. We analysed the presence of probable
RBD, sex, and the APOE ε4 carrier status as potential sub-group stratifiers.

**Methods**: A total of 400 iPD patients were included in the cross-sectional
analysis from the baseline dataset with a completed RBD Screening Questionnaire
(RBDSQ) for classifying as pRBD by using the cut-off RBDSQ ≥ 6. Multiple
regression models were applied to explore (i) the effects of pRBD on clinical
outcomes adjusted for disease duration and age, (ii) the effects of sex on pRBD
and (iii) the association of APOE ε4 and pRBD.

**Results**: iPD-pRBD was significantly associated with autonomic dysfunction
(SCOPA-AUT), level of depressive symptoms (BDI-I), MDS-UPDRS I, hallucinations
and constipation, whereas significantly negatively associated with quality of
life (PDQ-39) and sleep (PDSS). No significant association between sex and pRBD
or APOE ε4 and pRBD in iPD was found nor did we determine a significant effect
of APOE ε4 on the PD phenotype.

**Conclusion**: We identified an RBD-specific PD endophenotype, characterised
by predominant autonomic dysfunction, hallucinations and depression,
corroborating the concept of a distinctive body-first subtype of PD. We did not
observe a significant association between APOE ε4 and pRBD suggesting both
factors having an independent effect on cognitive decline in iPD.

{% endrtitle %}

{% rgridblock %}

{% rblock Data | fas fa-database %}

The dataset for this manuscript is not publicly available as it is linked to
the Luxembourg Parkinson’s Study and its internal regulations. Any requests for
accessing the dataset can be directed to <a href="mailto:request.ncer-pd@uni.lu">request.ncer-pd@uni.lu</a>.

{% endrblock %}

{% rblock source code %}

The source code is available <a href="https://gitlab.lcsb.uni.lu/TNG/papers/pRBD">here</a>.

{% endrblock %}

{% endrgridblock %}



{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | not yet published
{% endrfooter %}
