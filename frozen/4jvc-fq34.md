---
layout: default
date: 2023-03-28
order: -1
title: "Assessing the suitability of iPSC-derived human astrocytes for disease modeling"
permalink: /frozen/4jvc-fq34
published: true
doi: 10.17881/4jvc-fq34
type: manuscript
---
{% rtitle {{ page.title }} %}
Patrycja Mulica, Carmen Venegas, Zied Landoulsi, Katja Badanjak, Semra Smajic, Sylvie Delcambre, Jens Schwamborn, Rejko Krüger, Paul Antony, Patrick May, Enrico Glaab, Anne Grünewald, Sandro L. Pereira

Disease modeling with iPSC-derived cultures has proved to be particularly useful, although the selection of an appropriate protocol for a given research question remains challenging. Thus, here, we compared two approaches for the generation of iPSC-derived astrocytes. We phenotyped glia that were obtained using the differentiation protocols by Oksanen or Palm and colleagues, respectively. We employed high-throughput imaging and RNA sequencing to deep-characterize the cultures. Oksanen and Palm astrocytes differ considerably in their properties: while the former cells are more labor-intense in their generation (5 vs 2 months), they are also more mature. This notion was  strengthened by data resulting from cell type deconvolution analysis that was applied to bulk transcriptomes from the cultures to assess their similarity with human postmortem astrocytes. Overall, our analyses highlight the need to consider the advantages and disadvantages of a given differentiation protocol, when designing functional or drug discovery studies involving iPSC-derived astrocytes.

{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock Raw data %}
The raw data for this manuscript is not publicly available due to its sensitive nature. Any requests for accessing the data can be directed to [anne.gruenewald@uni.lu](mailto:anne.gruenewald@uni.lu).
{% endrblock %}

{% rblock Derived data %}
All supplementary data and data behind figures are available [here](https://webdav-r3lab.uni.lu/public/data/4jvc-fq34/).
{% endrblock %}

{% rblock Source code %}
The scripts used to analyse the data are available [here](https://gitlab.lcsb.uni.lu/MFN/camesyn/comparison-protocols).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | not yet published
{% endrfooter %}
