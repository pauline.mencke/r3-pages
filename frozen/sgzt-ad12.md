---
layout: default
date: 2021-07-06
order: -1
title: "Functional meta-omics provide critical insights into long and short read assemblies"
permalink: /frozen/sgzt-ad12
published: true
doi: 10.17881/sgzt-ad12
type: manuscript
manuscript: 10.1093/bib/bbab330
---

{% rtitle {{ page.title }} %}
Valentina Galata, Susheel Bhanu Busi, Benoit Josef Kunath, Laura de Nies, Magdalena Calusinska, Rashi Halder, Patrick May, Paul Wilmes, Cedric Christian Laczny
{% endrtitle %}

{% rgridblock a-unique-id %}
{% rblock  Manuscript %}
A preprint of the manuscript is available at <a href="https://www.biorxiv.org/content/10.1101/2021.04.22.440869v1">here</a>.
{% endrblock %}


{% rblock Data %}
The newly generated data is available under NCBI BioProject accession PRJNA723028 (Biosamples: metag_sr: SAMN18797629, metat_sr: SAMN18797630, and metag_lr: SAMN18797631). Metaproteomics is available at ProteomeXchange under accession PXD025505.

The previously published data is available at ERR2984773 and ERR2887847, SRR7585899 and SRR7585900, as well as ERR2027899, ERR4334939, ERR4334940, and ERR4334941 from the <a href="https://www.ebi.ac.uk/ena/browser/home">European Nucleotide Archive</a>
{% endrblock %}


{% rblock source code %}
The source code is available <a href="https://gitlab.lcsb.uni.lu/ESB/ont_pilot_gitlab">here</a>.
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
