---
layout: default
date: 2023-01-11
order: -1
title: "Premature senescence is associated with dopaminergic decline and α-synuclein pathology in a midbrain organoid model of Parkinson’s Disease"
permalink: /frozen/zmmj-8z40
published: true
doi: 
type: 
---
{% rtitle {{ page.title }} %}
Mudiwa Nathasia Muwanigwa, Jennifer Modamio Chamarro, Paul Antony, Silvia Bolognin, Jens Christian Schwamborn

Parkinson’s disease (PD) is a complex, progressive neurodegenerative disease characterized by the loss of dopaminergic neurons in the substantia nigra pars compacta. Here we use patient specific human midbrain organoids to investigate the molecular and cellular changes that precede neurodegeneration in PD. We investigate differences between wild type and 3xSNCA mutant organoids with respect to neural stem cell activity as well as neuronal, synaptic, and astrocytic development at 15, 21, 30, 50, 70 and 90 days of organoid maturation. We can recapitulate key hallmarks of PD, including the aggregation of α- synuclein and the decline of dopaminergic neurons. We found that these pathological hallmarks were associated with an increase in senescence associated cellular phenotypes including nuclear lamina defects, the presence of senescence associated heterochromatin foci and the upregulation of cell cycle arrest pathways. These results suggest a role of pathological α-synuclein in inducing senescence which may in turn increase the vulnerability of dopaminergic neurons to degeneration.
{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock Raw data %}
The raw data is available [here](https://webdav-r3lab.uni.lu/public/data/zmmj-8z40).
{% endrblock %}

{% rblock Source code %}
The scripts used to analyse the data are available [here](https://gitlab.lcsb.uni.lu/dvb/muwanigwa_2023).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}

{% endrfooter %}
