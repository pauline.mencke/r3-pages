---
layout: default
date: 2021-03-03
order: -1
title: "Retrospective Non-target Analysis to Support Regulatory Water Monitoring: From Masses of Interest to Recommendations via in silico workflows"
permalink: /frozen/40ss-ft75
published: true
doi: 10.17881/40ss-ft75
type: manuscript
manuscript: 10.1186/s12302-021-00475-1
---

{% rtitle {{ page.title }} %}
Adelene Lai, Randolph R. Singh, Lubomira Kovalova, Oliver Jaeggi, Todor Kondic, Emma L. Schymanski
{% endrtitle %}

{% rgridblock a-unique-id %}
{% rblock  Manuscript %}
The manuscript of the paper is available <a href="https://enveurope.springeropen.com/articles/10.1186/s12302-021-00475-1">here</a>.
{% endrblock %}


{% rblock Data %}
The data is available <a href="https://massive.ucsd.edu/ProteoSAFe/dataset.jsp?task=14f51e6ec99a42329e7a0eeaad0e5824">here</a>
{% endrblock %}


{% rblock source code %}
The source code is available <a href="https://gitlab.lcsb.uni.lu/eci/shinyscreen/-/tree/v.0.1.1-paper">here</a>.
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
