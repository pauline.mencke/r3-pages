---
layout: default
date: 2021-05-17
order: -1
title: "COBREXA.jl: constraint-based reconstruction andexascale analysis"
permalink: /frozen/zkcr-bt30
published: true
doi: 10.17881/zkcr-bt30
type: manuscript
manuscript: 10.1093/bioinformatics/btab782
---

{% rtitle {{ page.title }} %}

Miroslav Kratochvíl, Laurent Heirendt, St. Elmo Wilken, TaneliPusa, Sylvain Arreckx, Alberto Noronha, Marvin van Aalst, Venkata P. Satagopam, Oliver Ebenhöh, Reinhard Schneider, ChristopheTrefois, and Wei Gu

{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock source code | fas fa-code-branch %}
**COBREXA.jl** source code is available from [GitHub](https://github.com/LCSB-BioCore/COBREXA.jl).

**SBML.jl** source code is available from [GitHub](https://github.com/LCSB-BioCore/SBML.jl).

**DistributedData.jl** source code is available from [GitHub](https://github.com/LCSB-BioCore/DistributedData.jl).
{% endrblock %}

{% rblock documentation | fas fa-book %}
The documentation of **COBREXA.jl** is available [here](https://lcsb-biocore.github.io/COBREXA.jl).
{% endrblock %}

{% rblock benchmarks | fas fa-code %}
The benchmark scripts are available [here](https://gitlab.lcsb.uni.lu/R3/outreach/papers/cobrexa/benchmarks).
{% endrblock %}

{% rblock supplementary information | fas fa-code %}
A comparison of CPU performance used for the difference estimate in supplementary section 3.3 is available [at cpubenchmark.net](https://www.cpubenchmark.net/compare/Intel-i5-1038NG7-vs-Intel-Xeon-E5-2680-v4/3723vs2779).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
