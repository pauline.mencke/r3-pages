---
layout: default
date: 2022-05-12
order: -1
title: "SMASCH: multi-appointment scheduling system for longitudinal clinical research studies"
permalink: /frozen/8wvb-mt36
published: true
doi: 10.17881/8wvb-mt36
type: manuscript
manuscript: 10.1093/jamiaopen/ooac038
---


{% rtitle {{ page.title }} %}

Carlos Vega, Piotr Gawron, Jacek Lebioda, Valentin Grouès, Piotr Matyjaszczy, Claire Pauly, Ewa Smula, Rejko Krueger, Reinhard Schneider, Venkata Satagopam

Longitudinal clinical research studies require conducting various assessments over long periods of time. Such assessments comprise numerous stages, requiring different resources defined by multidisciplinary research staff and aligned with available infrastructure and equipment, altogether constrained by time. While it is possible to manage the allocation of resources manually, it is complex and error-prone. Efficient multi-appointment scheduling is essential to assist clinical teams, ensuring high participant retention and producing successful clinical studies, directly impacting patient throughput and satisfaction. This application note presents SMASCH (Smart Scheduling) system, a web application for multi-appointment scheduling management aiming to reduce times, optimise resources and secure personal identifiable information. SMASCH is present in multiple clinical research and integrated care programmes in Luxembourg since 2017, including Dementia Prevention Program, the study for Mild Cognitive Impairment and gut microbiome, and the National Centre of Excellence in Research on Parkinson’s disease which encompasses the study for REM sleep behaviour disorder and the Luxembourg Parkinson’s Study.
{% endrtitle %}

{% rgridblock a-unique-id %}

{% rblock Manuscript | fas fa-newspaper %}
The manuscript can be found under [doi:10.1093/jamiaopen/ooac038](https://doi.org/10.1093/jamiaopen/ooac038).
{% endrblock %}

{% rblock Code %}
The source code can be found on [Gitlab](https://gitlab.lcsb.uni.lu/smasch/scheduling-system).
{% endrblock %}

{% endrgridblock %}


{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
