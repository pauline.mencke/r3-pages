---
layout: default
date: 2020-10-12
order: -1
title: "Parkinson's Disease Phenotypes in Patient Neuronal Cultures and Brain Organoids Improved by 2‐Hydroxypropyl‐β‐Cyclodextrin Treatment"
permalink: /frozen/c80y-2k58
published: true
doi: 10.17881/c80y-2k58
type: manuscript
manuscript: 10.1002/mds.28810
---

{% rtitle {{ page.title }} %}
Javier Jarazo, Kyriaki Barmpa, Jennifer Modamio, Cláudia Saraiva, Sònia Sabaté-Soler, Isabel Rosety, Anne Griesbeck, Florian Skwirblies, Gaia Zaffaroni, Lisa M. Smits,Jihui Su, Jonathan Arias-Fuenzalida, Jonas Walter, Gemma Gomez-Giro, Anna S. Monzel, Xiaobing Qing, Armelle Vitali,  Gerald Cruciani, Ibrahim Boussaad, Francesco Brunelli,  Christian  Jäger, Aleksandar Rakovic, Wen Li, Lin Yuan, Emanuel Berger, Giuseppe Arena, Silvia Bolognin, Ronny Schmidt,Christoph Schröder, Paul M.A. Antony, Christine Klein, Rejko Krüger, Philip Seibler, Jens C.Schwamborn
{% endrtitle %}


{% rgridblock a-unique-id %}
{% rblock  Raw data | fas fa-video %}
The complete Dataset is available [here](https://webdav-r3lab.uni.lu/public/data/c80y-2k58/). It is subdivided into originals (raw data) and partials (analysis) specific to each figure and supplementary present in the manuscript.
{% endrblock %}


{% rblock source code %}
The scripts used to make the analysis/figures of the publication is available on [Github](https://gitlab.lcsb.uni.lu/dvb/Jarazo_2021).
{% endrblock %}

{% endrgridblock %}



{% rfooter %}
Date | {{ page.date }}
DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})
DOI type | Manuscript
Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})
{% endrfooter %}
