module Jekyll
    module Tags
      class RfooterTag < Liquid::Block

        def split_params(params)
            params.split("|").map(&:strip)
        end

        def initialize(tag_name, block_options, liquid_options)
            super
        end

        def render(context)
            rgridblockID = context["rgridblockID"]

            #content = super
            site = context.registers[:site]
            converter = site.find_converter_instance(::Jekyll::Converters::Markdown)
            content = converter.convert(super)

            #Because we’re not changing the context, there’s no need to push the context stack as with the rgridblock.
            # generate collapsible card HTML
            output = <<~EOS

<div class="rblock rblock_metadata"#{@block_id_html}>
    <h3>
    <i class="fas fa-sitemap"></i>
    Metadata
    </h3>
    <hr>
    <p>
    #{content}
    </p>
</div>
    EOS
             output
        end
      end
    end
  end

Liquid::Template.register_tag('rfooter', Jekyll::Tags::RfooterTag)