---
layout: default
title: Websites
order: 3
permalink: /websites/
---
# Websites hosted under R3

    {% comment %}
        Please note that the following websites are contained in their own repositories
    {% endcomment %}

* **[TGM pipeline]({{ "web/tgm-pipeline/" | relative_url }})** - a platform for visually-aided exploration, analysis and interpretation of high-throughput translational medicine data

* **[IMP]({{ "web/imp/" | relative_url }})** - Integrated Meta-omic Pipeline

* **[MINERVA](https://minerva.pages.uni.lu/doc/)** - a platform for Molecular Interaction NEtwoRk VisuAlization

* **[SYSCID](http://datacatalog.elixir-luxembourg.org/limesurvey/index.php?r=survey/index/sid/395386&lang=en)** - Data Management Plan Information Collection

* **[CaSiAn]({{ "web/casa/" | relative_url }})** - CaSiAn Website

* **[MIC-MAC](https://isc.pages.uni.lu/micmac/)** - A Pipeline for High-throughput Microglia Morphology Analysis
