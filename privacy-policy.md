---
layout: default
title: Privacy Policy for the LCSB Courses Platform
permalink: /privacy-policy

---

# Privacy Policy the R3 lab website
{:.no_toc}

<br/>
* TOC
{:toc}
<br/>

<a name="PRF"></a>
# 1. Preface

This is the privacy policy for the [R3 lab website](https://r3lab.uni.lu) that is hosted by the University of Luxembourg’s Luxembourg Centre for Systems Biomedicine (UL-LCSB) the **data controller** here on referred to as "we", "us", "our".
In this policy we outline what **personal data** ("data") we collect when **data subjects** ("you", "your") visit our sites and use our services, for what purposes this data is collected, where and how long it is kept and your rights regarding data as per the EU General Data Protection Regulation ("GDPR"). We also list relevant contact persons regarding requests and inquiries on data and data protection.
The policy document is organised as follows. In [Section 2](#SEC2) we provide contact details for the data controller. In [Section 3](#SEC3) we list your rights regarding the personal data collected. In [Section 4](#SEC4) we summarise the measures taken to protect personal data. In [Section 5](#SEC5) we list the personal data commonly collected on our website. We outline where the data is stored, for what purposes the data is used and the lawful basis for us to process the data. In [Section 5](#SEC5) you will also find our sites' cookie policy.

<a name="SEC2"></a>
# 2. Who is the data controller?

The data controller is the University of Luxembourg.

 _University of Luxembourg,_ <br/>
 _2, avenue de l'Université,_  <br/>
 _L-4365 Esch-sur-Alzette._<br/>


University of Luxembourg’s Data Protection Officer’s contact information is given below.
This contact should be used to exercise of your rights regarding personal data and also for general inquiries on data protection.

 _Dr Sandrine Munoz,_ <br/>
 _[dpo (AT) uni (dot) lu](mailto:dpo@uni.lu)_ <br/>
 _University of Luxembourg,_ <br/>
 _Legal Affairs, Central Administration,_ <br/>
 _Maison du Savoir,_ <br/>
 _L-4365 Esch-sur-Alzette._ <br/>

<a name="SEC3"></a>
# 3. What are your rights regarding the data we collect?

As per GDPR, you as a “data subject” have rights on your personal data. <br/> <br/>
You have the right to be informed that UL-LCSB is processing your personal information.<br/><br/>
You have the right to access your personal information and in case it is inaccurate or incomplete you have the right to have it rectified without undue delay.<br/><br/>
You have the right to ask that we delete your personal data or restrict its use. Where applicable, you have the right to object to our processing of your personal data, and the right to data portability. Your requests for deletion and processing restriction will be assessed by us and we will notify you of the result of this assessment within one month of receipt of the request. This period may be extended by two further months where necessary, taking into account the complexity and the number of applications (in accordance with article 12.3 GDPR).<br/><br/>
You can request that we notify you in case any changes are made to your personal data or your data has been sent to any other party.<br/><br/>
You have the right to lodge a complaint with the Luxembourgish data protection supervisory authority, CNPD, in case you consider that our processing of your personal data infringes the GDPR.<br/><br/>

In order to exercise any of the above rights, you shall contact the University of Luxembourg’s Data Protection Officer in writing. The procedure for this is described in detail [here](https://wwwen.uni.lu/university/data_protection/your_rights).<br/><br/>


<a name="SEC4"></a>
# 4. How do we protect your data?

The UL-LCSB has put in place a number of organisational and technical measures for the protection of your personal data in compliance with the EU GDPR. These measures include but are not limited to access control, encrypted data transmission, institutional policies, staff code of conduct and training on data protection.
UL-LCSB takes all measures reasonably necessary to protect against unauthorized access, use, alterations or destruction of your personal data. Only authorized personnel at the UL-LCSB and its host institution can access the data. Examples of such personnel include, but is not limited to, UL and UL-LCSB IT system administrators.

The types of personal data collected on our sites, its storage, use and sharing is described in the following sections. Please note that:
 - No personal data collected on the R3 lab website are used for direct marketing purposes.
 - The data collected on the platform will not be forwarded to 3rd parties.

<a name="SEC5"></a>
# 5. Which personal data is collected?

## 5.1 Site visitor information and site analytics

**Data and its purpose of use**

When you visit the [R3 lab website](https://r3lab.uni.lu), we collect the following information:

 - your IP address,
 - your device type, name and ID,
 - your browser version,
 - your operating system and language settings,
 - date and time of web resource access request,
 - content of access request,
 - status and size of response to a request,

This information is automatically collected by server-side software that deliver pages to site visitors.
We use this data to analyse site usage, which in turn allows us to further optimize and secure our site.
The data will be used for statistical purposes only.

In addition, only if you agree to the use of site analytics, we will collect the following information during your visit of our site:
 - visitor id, which is a unique string calculated based on your IP address (two bytes masked), operating system, browser, browser plugins, and browser language,
 - the URLs, and titles of pages you visit on our site as well as custom events on our pages (such as links clicked),
 - referrer page, which is the web page such as a search engine or newsletter link, that leads your visit to our site.

Site analytics information allows us to optimise our site content and its delivery. We use this information to determine whether you have visited our website before, what you have previously viewed or clicked on, how user-friendly our forms are and how you found us. The information is anonymous and only used for statistical purposes, and it helps us to analyse patterns of user activity and to develop a better user experience.  We also use this information to prepare aggregated, anonymous statistical reports of visitor activity needed for performance evaluation.

**Legal basis**

The site visitor and site analytics information are necessary for us in order to deliver the website to you as well as to guarantee the website’s stability and security; therefore it is in our legitimate interest (GDPR Article 6(1)(f)) to collect and use site visitor information.

**Storage location and duration**

We store site visitor and site analytics data on servers located in Luxembourg, specifically at the University of Luxembourg’s data centre, for a maximum period of 12 months. Visitor logs are deleted after this period, aggregated statistics on site visits are stored for an indefinite period.

**Transfers**

Site visitor and site analytics information collected by our server-side software is not transferred to any other country.

## 5.2 Site cookies

**Data and its purpose of use**

[R3 lab website](https://r3lab.uni.lu) makes use of cookies. Cookies are bits of information that are created and maintained by your web browser when you visit a website. Cookies are categorized as first-party and third-party. First-party cookies are placed by the website you visit and will not track your activity once you leave that website. Third-party cookies are often set by another site not the one you’re visiting, e.g. advertisements, social media widgets, and they may continue to track your activity across sites. Our site is designed to use the cookies listed below:

 - Cookie preference  <br/> When you visit our sites for the first time you will be notified of our cookie policy. You will have the option to proceed only with technically necessary cookies (i.e. optimisation, security and authentication) or (additionally) allow site analytics cookies. We will keep your preference in a cookie named “lap”. Your cookie preferences will be displayed at the bottom of pages, and you will have the option to
 change your cookie settings at any time.

 - Optimization and security cookies<br/> Our sites are fronted by load balancers and firewalls managed by the University of Luxembourg. As a result, additional cookies named "LBServer" and "TS*" will be placed in your browser.

Optimization and security, authentication cookies are first-party and there is no personal information stored in them.

In addition, our websites make use of the following optional cookies:

 - Analytics cookies (Optional)<br/> If you allow the use of site analytics, our site will place the following first-party analytics cookies: "_pk_id*" ,"_pk_ses*", "_pk_cvar", "_pk_hsr" and "_pk_ref".
    - "_pk_id*" holds your unique visitor ID.
    - "_pk_ref" holds the referrer page information, where applicable.
    - "_pk_ses*", "_pk_cvar" and "_pk_hsr" are short-lived cookies used to temporarily store data for a particular visit of you to our site.


**Storage location and duration**

Normally cookies are stored on your computer until the end of their expiry period summarized below. You may also choose to clear your browser cookies manually.

| Cookie category | Cookie name| Expiry period |
|--------------------|--------------------|--------------------|
| Cookie preference  | lap| 6 months|
| Cookie preference | MATOMO_SESSID | Temporarily created when user opts out of analytics |
| Optimisation & Security | LBServer | End of browsing session |
| Optimisation & Security | TS* | End of browsing session |
| Analytics (optional) | _pk_id* | 1 year |
| Analytics (optional) | _pk_ref | 6 months |
| Analytics (optional) | _pk_ses*, _pk_cvar, _pk_hsr| End of browsing session |


**Legal basis**

The use of cookies is in our legitimate interest (GDPR Article 6(1)(f)), as cookies enable us to deliver our websites to you in a stable and secure manner.

**Transfers**

Essential cookies i.e. the Optimisation & Security cookies listed above are required for our websites to function. These cookies are stored on your computer, and information in them is transferred to servers in Luxembourg, when cookie information is used by our site servers.

<br/><br/>
Last updated: 28 June 2021

