---
layout: default
title: Frozen pages
order: 2
permalink: /frozen/
---
# Frozen pages

Here you can find frozen pages that are related to a published scientific paper. A frozen page regroups all links to all components referenced in the paper and is not modified after the release of the paper.

This website is under version control on the [LCSB Gitlab](https://gitlab.lcsb.uni.lu/R3/r3-pages).

<!-- index -->

<h3>2023</h3>

<ul>
	<li><a href="{{ '/frozen/4jvc-fq34' | relative_url }}">Assessing the suitability of iPSC-derived human astrocytes for disease modeling</a></li>
	<li><a href="{{ '/frozen/77zg-bc41' | relative_url }}">Seizure-induced increase in microglial cell population in the developing zebrafish brain</a></li>
	<li><a href="{{ '/frozen/zmmj-8z40' | relative_url }}">Premature senescence is associated with dopaminergic decline and α-synuclein pathology in a midbrain organoid model of Parkinson’s Disease</a></li>
	<li><a href="{{ '/frozen/0gdr-7705' | relative_url }}">Cross-domain interactions induce community stability to benthic biofilms in proglacial streams</a></li>

</ul>
<h3>2022</h3>

<ul>
	<li><a href="{{ '/frozen/hczj-3297' | relative_url }}">Penalised regression with multiple sources of prior effects</a></li>
	<li><a href="{{ '/frozen/fpn5-th91' | relative_url }}">Ice Recrystallization Inhibitors enable efficient cryopreservation of induced pluripotent stem cells: a functional and transcriptomic analysis</a></li>
	<li><a href="{{ '/frozen/frps-jn50' | relative_url }}">ENA Dataset for Systematic characterization of human gut microbiome-secreted molecules by integrated multi-omics study</a></li>
	<li><a href="{{ '/frozen/sw04-1w80' | relative_url }}">Body-first subtype of Parkinson's disease with probable REM-sleep behaviour disorder is associated with non-motor dominant phenotype</a></li>
	<li><a href="{{ '/frozen/8wvb-mt36' | relative_url }}">SMASCH: multi-appointment scheduling system for longitudinal clinical research studies</a></li>
	<li><a href="{{ '/frozen/9p51-ch19' | relative_url }}">ChemPert: mapping between chemical perturbation and transcriptional response for noncancer cells</a></li>
	<li><a href="{{ '/frozen/hr67-ba06' | relative_url }}">Age at onset as stratifier in idiopathic Parkinson's disease – effect of ageing and polygenic risk score on clinical phenotypes</a></li>

</ul>
<h3>2021</h3>

<ul>
	<li><a href="{{ '/frozen/w2d6-4934' | relative_url }}">Concomitant AD and DLB pathologies shape subfield microglia responses in the hippocampus</a></li>
	<li><a href="{{ '/frozen/hpbx-y095' | relative_url }}">Meta-analysis of gender-dependent gene expression alterations in Parkinson's disease</a></li>
	<li><a href="{{ '/frozen/y9k6-xa72' | relative_url }}">The Parkinson's disease associated mutation LRRK2-G2019S alters dopaminergic differentiation dynamics via NR2F1</a></li>
	<li><a href="{{ '/frozen/cca2-s098' | relative_url }}">Single cell transcriptomics of human iPSC differentiation dynamics reveal a core network of Parkinson’s disease</a></li>
	<li><a href="{{ '/frozen/9xkm-3s60' | relative_url }}">Dibac: Distribution-Based Analysis Of Cell Differentiation Identifies Mechanisms Of Cell Fate</a></li>
	<li><a href="{{ '/frozen/1yzp-qv41' | relative_url }}">Synaptic decline precedes dopaminergic neuronal loss in human midbrain organoids harboring a triplication of the SNCA gene</a></li>
	<li><a href="{{ '/frozen/sgzt-ad12' | relative_url }}">Functional meta-omics provide critical insights into long and short read assemblies</a></li>
	<li><a href="{{ '/frozen/255d-4a98' | relative_url }}">Method optimization of skin biopsy-derived fibroblast culture for reprogramming into induced pluripotent stem cells (iPSCs)</a></li>
	<li><a href="{{ '/frozen/zkcr-bt30' | relative_url }}">COBREXA.jl: constraint-based reconstruction andexascale analysis</a></li>
	<li><a href="{{ '/frozen/rc4f-nk07' | relative_url }}">Midbrain organoids mimic early embryonic neurodevelopment and recapitulate LRRK2-G2019S - associated gene expression</a></li>
	<li><a href="{{ '/frozen/g9aq-jy72' | relative_url }}">Identification of tissue-specific and common methylation quantitative trait loci in healthy individuals using MAGAR</a></li>
	<li><a href="{{ '/frozen/40ss-ft75' | relative_url }}">Retrospective Non-target Analysis to Support Regulatory Water Monitoring: From Masses of Interest to Recommendations via in silico workflows</a></li>
	<li><a href="{{ '/frozen/cx25-ht49' | relative_url }}">Microglia integration into human midbrain organoids leads to increased neuronal maturation and functionality</a></li>
	<li><a href="{{ '/frozen/7bwb-aj16' | relative_url }}">Retrograde procedural memory in Parkinson's disease: a cross-sectional, case-control study</a></li>

</ul>
<h3>2020</h3>

<ul>
	<li><a href="{{ '/frozen/tnyy-fy53' | relative_url }}">Metadata for the RESOLUTE project in the IMI Data Catalog</a></li>
	<li><a href="{{ '/frozen/th9v-xt85' | relative_url }}">PRECISESADS: Molecular reclassification to find clinically useful biomarkers for systemic autoimmune diseases</a></li>
	<li><a href="{{ '/frozen/q3g1-7a85' | relative_url }}">Agent-based SEIR-ICU model for Luxembourg</a></li>
	<li><a href="{{ '/frozen/c80y-2k58' | relative_url }}">Parkinson's Disease Phenotypes in Patient Neuronal Cultures and Brain Organoids Improved by 2‐Hydroxypropyl‐β‐Cyclodextrin Treatment</a></li>
	<li><a href="{{ '/frozen/j20h-pa27' | relative_url }}">PaFSe: a Parameter Free Segmentation Approach for 3D Fluorescent Images</a></li>
	<li><a href="{{ '/frozen/z5vy-fa75' | relative_url }}">Clustering and visualizing huge-scale cytometry datasets with GigaSOM.jl</a></li>
	<li><a href="{{ '/frozen/lcsb-kcqg-tr55' | relative_url }}">Using High-Content Screening to Generate Single-Cell Gene-Corrected Patient-Derived iPS Clones Reveals Excess Alpha-Synuclein with Familial Parkinson’s Disease Point Mutation A30P</a></li>
	<li><a href="{{ '/frozen/lcsb-a8hh-1022' | relative_url }}">Passive controlled flow for neuronal cell culture in 3D microfluidic devices</a></li>

</ul>
<h3>2019</h3>

<ul>
	<li><a href="{{ '/frozen/lcsb-20191008-01' | relative_url }}">Reproducible generation of human midbrain organoids for in vitro modeling of Parkinson's disease</a></li>
	<li><a href="{{ '/frozen/lcsb-2019130913-01' | relative_url }}">Synapse alterations precede neuronal damage and storage pathology in a human cerebral organoid model of CLN3-juvenile neuronal ceroid lipofuscinosis</a></li>
	<li><a href="{{ '/frozen/lcsb-20191309-02' | relative_url }}">Machine learning-assisted neurotoxicity prediction in human midbrain organoids</a></li>
	<li><a href="{{ '/frozen/lcsb-20190507-01' | relative_url }}">Mitochondrial morphology provides a mechanism for energy buffering at synapses</a></li>
	<li><a href="{{ '/frozen/lcsb-20190326-01' | relative_url }}">Single-cell transcriptomics reveals multiple neuronal cell types in human midbrain-specific organoids</a></li>
	<li><a href="{{ '/frozen/lcsb-20192701-01' | relative_url }}">Modeling Parkinson's disease in midbrain-like organoids</a></li>

</ul>
<h3>2016</h3>

<ul>
	<li><a href="{{ '/frozen/21vm-kj21' | relative_url }}">IMP: a pipeline for reproducible reference-independent integrated metagenomic and metatranscriptomic analyses</a></li>

</ul>
