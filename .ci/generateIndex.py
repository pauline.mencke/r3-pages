from asyncore import loop
import os, re
from os import path
from pathlib import Path
from datetime import datetime

def line_appender(filename, date, doi, manuscript, type):

    line = "\n\n{% rfooter %}\n"

    # add date
    if len(date) > 0:
        line += "Date | {{ page.date }}\n"
    else:
        print(" * Metadata 'date' missing")

    # add doi
    if len(doi) > 0:
        line += "DOI | [doi:{{ page.doi }}](https://doi.org/{{ page.doi }})\n"
    else:
        print(" * Metadata 'doi' missing")

    # add type
    if len(type) > 0:
        if type == 'manuscript':
            line += "DOI type | Manuscript\n"
            if len(manuscript) > 0:
                line += "Manuscript | [{{ page.title }}](https://doi.org/{{ page.manuscript }})"
            else:
                line += "Manuscript | not yet published"
        elif type == 'data':
            line += "DOI type | Dataset"
        else:
            line += "DOI type | Other"
    else:
        print(" * Metadata 'type' missing")

    line += "\n{% endrfooter %}"

    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(content + line.rstrip('\r\n') + '\n')

def line_prepender(filename, line):
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)

def build_link(title, href):

    # add relative url
    href = "{{ '" + href + "' | relative_url }}"

    title = title.replace('"', ' ').rstrip().lstrip()

    return '\t<li><a href="' + href + '">' + title + '</a></li>\n'

def save_tag(localroot, root, filename, tag):
    return_tag = ""
    os.chdir(root)

    length_tag = len(tag)

    with open(filename, 'r') as f:
        for line in f:
            # check for the start of the section
            if line[0:length_tag+1] == tag + ":":
                return_tag = line[length_tag+2:]
                break

    # change back to the local root
    os.chdir(localroot)

    return return_tag.rstrip()

def save_redirect_from(localroot, root, filename):
    legacy_from = []
    os.chdir(root)

    count = 0
    legacy_from_flag = False
    tag = "redirect_from"
    length_tag = len(tag)
    with open(filename, 'r') as f:
        for line in f:
            count += 1
            # check for the start of the section
            if line[0:length_tag+1] == tag + ":":
                legacy_from_flag = True

            # append lines from the legacy section
            if legacy_from_flag:
                legacy_from.append(line)

            # check for the end of the header
            if legacy_from_flag and line[0:3] == "---":
               legacy_from_flag = False
               break

    # change back to the local root
    os.chdir(localroot)

    return legacy_from

def remove_metadata(localroot, root, filename):

    os.chdir(root)

    # count the number of lines
    lines = []
    with open(filename, 'r') as f:
        for line in f:
            lines.append(line)

    print(lines)
    tag_start = "{% rfooter %}\n"

    if tag_start in lines:
        print(lines.index(tag_start))

        lines_chopped = lines[0:lines.index(tag_start)-2]

        # remove the header
        with open("tmp"+filename, 'w') as f:
            for line in lines_chopped:
                f.write(line)
        os.remove(filename)
        os.rename("tmp"+filename, filename)
        print(" - Old metadata removed.")

    # change back to the local root
    os.chdir(localroot)


def remove_header(localroot, root, filename):
    nfirstlines = []

    os.chdir(root)

    # count the number of lines
    count = 0
    n = 0
    headerCheck = False
    with open(filename, 'r') as f:
        for line in f:
            count += 1

            # check if the header is actually a header
            if count > 1 and line[0:3] == "---":
                headerCheck = True
                n = count

    # remove the header
    if headerCheck:
        with open(filename) as f, open("tmp"+filename, "w") as out:
            for _ in range(n):
                nfirstlines.append(next(f))
            for line in f:
                out.write(line)

        os.remove(filename)
        os.rename("tmp"+filename, filename)
        print(" - Old header removed.")

    # change back to the local root
    os.chdir(localroot)

    return n

def generate_header(folder, permalink, order, legacy_from, title, description, published, date, doi, manuscript, type):
    header = "---\n"
    header += "layout: default\n"

    if len(date) > 0:
        header += "date: " + date + "\n"
    if len(order) > 0:
        header += "order: " + str(order) + "\n"

    # add the title and description
    if len(title) > 0:
        header += "title: " + title + "\n"
    if len(description) > 0:
        header += "description: " + description + "\n"

    header += "permalink: " + permalink + "\n"
    header += "published: " + published + "\n"
    header += "doi: " + doi + "\n"
    header += "type: " + type + "\n"
    if type == "manuscript" and len(manuscript) > 0:
        header += "manuscript: " + manuscript + "\n"

    # include the legacy section
    if len(legacy_from) > 0:
        for item in legacy_from:
            header += str(item)
    else:
        header += "---"

    return header

# loop through the entire internal tree
localroot = os.getcwd()

# generate the index properly speaking
cardDirs = ["frozen"]

# Index contains the generated content, init it with an empty container
index = ''
list_titles = []

for folder in cardDirs:
    # FolderFlag gets set to true at the first iteration
    folderFlag = True
    indexS = 0
                # walk through the folders with all the cards
    for root, dirs, files in os.walk(folder):
        for file in files:
            if file.endswith(".md"):
                fileName = os.path.join(root, file)
                # ignore subsections (.md files that start with _)
                if file[0] != "_":
                    print(" > Generating header for: " + fileName)

                    # save order and legacy section
                    order = save_tag(localroot, root, file, "order")
                    legacy_from = save_redirect_from(localroot, root, file)
                    title = save_tag(localroot, root, file, "title")
                    description = save_tag(localroot, root, file, "description")
                    permalink = save_tag(localroot, root, file, "permalink")
                    published = save_tag(localroot, root, file, "published")
                    date = save_tag(localroot, root, file, "date")
                    year = date[0:4]
                    doi = save_tag(localroot, root, file, "doi")
                    manuscript = save_tag(localroot, root, file, "manuscript")
                    type = save_tag(localroot, root, file, "type")

                    # remove the previous header
                    n = remove_header(localroot, root, file)
                    n1 = remove_metadata(localroot, root, file)

                    # generate the header for each card
                    header = generate_header(folder, permalink, order, legacy_from, title, description, published, date, doi, manuscript, type)

                    # add the header properly speaking
                    line_prepender(fileName, header)

                    # add metadata
                    line_appender(fileName, date, doi, manuscript, type)

                    # open file and get the title after the header
                    list_titles.append({ 'date' : date , 'year': year, 'title': title, 'link': build_link(title, permalink)})
                    # output
                    print(" + New header added.")
                    print("-----------------------")


# ordering of cards
list_titles.sort(key=lambda x: x['date'].split('-'), reverse=True)

# determine the index
loop_year = list_titles[0]['year']

# add the first year
index += "\n<h3>" + loop_year + "</h3>\n"
index += '\n<ul>\n'
# loop through all pages
for k in range(len(list_titles)):
    if list_titles[k]['year'] != loop_year:
        loop_year = list_titles[k]['year']
        index += "\n</ul>"
        index += "\n<h3>" + loop_year + "</h3>\n"
        index += '\n<ul>\n'

    # add the link properly speaking
    index += ''.join(list_titles[k]['link'])

index += '\n</ul>\n'

# Read in the file
indexFile = "frozen.md"
filedata = ""
with open(indexFile, 'r') as file :
    for line in file:
        filedata += line

        # stop reading once the index place holder has been reached
        if re.search("<!-- index -->", line):
            filedata += "[[ index ]]"
            break

# Replace the target string
filedata = filedata.replace('[[ index ]]', index)

# Write the file out again
with open(indexFile, 'w') as file:
  file.write(filedata)

print("\n > New index generated and saved in " + indexFile)